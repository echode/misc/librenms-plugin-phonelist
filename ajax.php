<?php

require 'config.php';
require '../../../includes/init.php';

if (!isset($_SERVER["HTTP_HOST"])) {
  parse_str($argv[1], $_POST);
}

$page = (int)$_POST['current'];
$max = (int)$_POST['rowCount'];
$start = 0;

$sql = "SELECT l.*, d.sysname, d.status, p.ifName, p.ifAlias, p.ifVlan, p.ifOperStatus, sv.vlan_name, mv.vlan_name AS vlan_name_master, ip.ipv4_address, lc.location, d.location_id FROM links l JOIN devices d ON d.device_id = l.local_device_id JOIN ports p ON p.port_id = l.local_port_id LEFT JOIN vlans sv ON sv.vlan_vlan = p.ifVlan AND sv.device_id = p.device_id LEFT JOIN locations lc ON lc.id = d.location_id LEFT JOIN vlans mv ON mv.vlan_vlan = p.ifVlan AND mv.device_id = '".$vlanmasterdevice."' LEFT JOIN ipv4_mac ip ON ip.port_id = l.local_port_id OR ip.mac_address = SUBSTRING(l.remote_port, -13, 12) WHERE l.remote_hostname REGEXP '".$hostnameregexp."' OR l.remote_port REGEXP '".$remoteportregexp."' ORDER BY l.remote_port";

$objects = array();
foreach (dbFetchRows($sql) as $row) {
  $object = array();
  $object['device_name'] = $row['remote_hostname'];
  $object['device_port'] = $row['remote_port'];
  $object['device_addr'] = $row['ipv4_address'];
  if ($row['remote_port_id'] != '') {
    $object['device_name'] = array();
    $object['device_name']['text'] = $row['remote_hostname'];
    $object['device_name']['url'] = '/device/'.$row['remote_device_id'];
    $object['device_port'] = array();
    $object['device_port']['text'] = $row['remote_port'];
    $object['device_port']['url'] = '/device/device='.$row['remote_device_id'].'/tab=port/port='.$row['remote_port_id'];
  }
  $object['connection_loc'] = array();
  $object['connection_loc']['text'] = $row['location'];
  $object['connection_loc']['url'] = '/devices/location='.$row['location_id'];
  $object['connection_name'] = array();
  $object['connection_name']['text'] = $row['sysname'];
  $object['connection_name']['url'] = '/device/'.$row['local_device_id'];
  $object['connection_port'] = array();
  $object['connection_port']['text'] = $row['ifName'].' ('.$row['ifAlias'].')';
  $object['connection_port']['url'] = '/device/device='.$row['local_device_id'].'/tab=port/port='.$row['local_port_id'];
  $object['connection_port']['status'] = $row['ifOperStatus'];
  $object['connection_vlan'] = $row['ifVlan'].' '.$row['vlan_name'];
  if ($vlanmasterdevice != 0 and $vlanmasterdevice != '0') {
    $object['connection_vlan'] .= ' ('.$row['vlan_name_master'].')';
  }
  $objects[] = $object;
}

function getstring($value,$param='text') {
  if (is_array($value)) {
    return strtolower($value[$param]);
  }
  return strtolower($value);
}

function sortobjects($sort) {
  if (!is_array($_POST['sort'])) { return $sort; }
  $keys = array_keys($_POST['sort']);
  foreach (array_reverse($keys) as $key) {
    usort($sort, function($a, $b) use ($key) {
      $stringa = getstring($a[$key]);
      $stringb = getstring($b[$key]);
      if ($_POST['sort'][$key] == 'desc') {
        return strcmp($stringb, $stringa);
      }
      return strcmp($stringa, $stringb);
    });
  }
  return $sort;
}

function filtervalues($value) {
  $search = strtolower($_POST['searchPhrase']);
  $string = getstring($value);
  return str_contains($string, $search);
}

function filterobjects($object) {
  $matches = array_filter($object, "filtervalues");
  $count = count($matches);
  if ($count > 0) { return true; }
  return false;
}

$total = count($objects);
if ($max < 1) { $max = $total; }
else { $start = ($page-1)*$max; }

$filtered = array_filter($objects, "filterobjects");
$sorted = array_values(sortobjects($filtered));
$paginated = array_slice($sorted, $start, $max);

$output = array();
$output['current'] = $page;
$output['total'] = count($filtered);
$output['rowCount'] = count($paginated);
$output['rows'] = array_values($paginated);

print json_encode($output);

?>
