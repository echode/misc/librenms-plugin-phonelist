<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default panel-condensed">
        <div class="panel-heading">
          <strong>Phone List</strong>
        </div>
        <table id="phone-list" class="table table-hover table-condensed table-striped">
          <thead>
            <tr>
              <th data-column-id="device_name" data-formatter="link">Device Name</th>
              <th data-column-id="device_port" data-formatter="link" data-sortable="false">Device Port</th>
              <th data-column-id="device_addr" data-sortable="false">Device IP</th>
              <th data-column-id="connection_name" data-formatter="link">Remote Name</th>
              <th data-column-id="connection_port" data-formatter="link" data-sortable="false">Remote Port</th>
              <th data-column-id="connection_vlan">Remote VLAN</th>
              <th data-column-id="connection_loc" data-formatter="link">Remote Location</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
<script>
  $("#phone-list").bootgrid({
    rowCount: [-1,250,100,50,25],
    columnSelection: true,
    caseSensitive: false,
    multiSort: false,
    ajax: true,
    url: "/plugins/PhoneList/ajax.php",
    formatters: {
      "link": function(column,row) {
        if (typeof row[column.id] === 'object' && row[column.id]['text'] != undefined) {
          let output = row[column.id]['text']
          if (row[column.id]['url'] != undefined) {
            let cssclass = ''
            if (column.id == "device_name") { cssclass = 'list-device' }
            output = '<a href="' + row[column.id]['url'] + '" class="' + cssclass + '">' + row[column.id]['text'] + '</a>'
          }
          if (row[column.id]['status'] == "down") { 
            output += '<i class="fa fa-flag fa-lg" style="color:red" aria-hidden="true" title="Remote Port is down"></i>'
          }
          return output
        }
        return row[column.id]
      }
    }
  });
</script>
