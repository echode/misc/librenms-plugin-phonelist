# librenms-plugin-PhoneList

A LibreNMS plugin to show a list of all IP Phones found by lldp/cdp and which vlan they are connected to.

This plugin was made to fit our environment explicitly, so it might not fit your needs out of the box. 

_Made for Aurskog-Høland kommune._


# Install

Rename and copy this directory to librenms/html/plugins/PhoneList.

Edit the config.php file to fit reflect your environment.

In LibreNMS go to Overview->Plugins->Plugin Admin.

Click Enable on "PhoneList".


# Usage

Go to Overview->Plugins->Phone List.